import react from '@vitejs/plugin-react';
import { defineConfig } from 'vite';
import eslintPlugin from 'vite-plugin-eslint';
import relay from 'vite-plugin-relay';

export default defineConfig({
  server: { open: process.env.OPEN_BROWSER === 'true' },
  build: { sourcemap: true },
  plugins: [
    react(),
    relay,
    eslintPlugin({
      exclude: ['**/node_modules/**', '**/__generated__/**'],
    }),
  ],
});
