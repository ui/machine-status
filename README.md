# Machine Status

Widgets and dashboards for **monitoring the status of the EBS**.

This standalone, web-based application means to eventually replace, notably:

- the [_Machine Status_](https://www.esrf.fr/Accelerators/Status) page on the
  ESRF website, which was built with JYSE, a software platform that is no longer
  maintaiend;
- the _Synopsis_ – i.e. the dashboard that is streamed to numerous TV screens
  throughout the ESRF.

_Machine Status_ is developed in React and TypeScript and uses
[Relay](https://relay.dev/) to communciate with a
[TangoGraphQL](https://gitlab.com/tango-controls/TangoGraphQL) server.
