import { ErrorBoundary } from 'react-error-boundary';

import styles from './App.module.css';
import { useLiveAttributesSubscription } from './data/hooks';
import ErrorFallback from './shared/ErrorFallback';
import BeamCurrent from './widgets/BeamCurrent';
import Beamlines from './widgets/Beamlines';
import CurrentDate from './widgets/CurrentDate';
import HistoricalCharts from './widgets/HistoricalCharts';
import Messages from './widgets/Messages';
import Status from './widgets/Status';

function App() {
  useLiveAttributesSubscription();

  return (
    <div className={styles.root}>
      <header className={styles.header}>
        <h1 className={styles.title}>ESRF-EBS Operation Status</h1>
        <CurrentDate />
      </header>
      <ErrorBoundary FallbackComponent={ErrorFallback}>
        <BeamCurrent />
        <Status />
        <Beamlines />
        <Messages />
        <HistoricalCharts />
      </ErrorBoundary>
    </div>
  );
}

export default App;
