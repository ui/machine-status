import { Suspense } from 'react';

import { FacadeAttribute } from '../data/attributes';
import Chart from '../shared/Chart';
import ChartDataLoader from '../shared/ChartDataLoader';
import styles from './HistoricalCharts.module.css';

function HistoricalCharts() {
  return (
    <div className={styles.root} data-span-2-cols>
      <Suspense fallback={<>Loading...</>}>
        <ChartDataLoader attribute={FacadeAttribute.CurrentHist}>
          {(values) => <Chart values={values} label="Current" unit="mA" />}
        </ChartDataLoader>
        <ChartDataLoader attribute={FacadeAttribute.LifetimeHist}>
          {(values) => {
            const processedValues = values.map((histState) => ({
              ...histState,
              value: histState.value / 60 / 60,
            }));
            return <Chart values={processedValues} label="Lifetime" unit="h" />;
          }}
        </ChartDataLoader>
        <ChartDataLoader attribute={FacadeAttribute.EmittanceHHist}>
          {(values) => {
            const processedValues = values.map((histState) => ({
              ...histState,
              value: histState.value * 1000,
            }));
            return (
              <Chart values={processedValues} label="Emittance H" unit="pm" />
            );
          }}
        </ChartDataLoader>
        <ChartDataLoader attribute={FacadeAttribute.EmittanceVHist}>
          {(values) => {
            const processedValues = values.map((histState) => ({
              ...histState,
              value: histState.value * 1000,
            }));
            return (
              <Chart values={processedValues} label="Emittance V" unit="pm" />
            );
          }}
        </ChartDataLoader>
      </Suspense>
    </div>
  );
}

export default HistoricalCharts;
