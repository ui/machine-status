import dayjs from 'dayjs';
import duration from 'dayjs/plugin/duration';

import { FacadeAttribute } from '../data/attributes';
import { useLiveAttributes } from '../data/hooks';
import type { ColorStop } from '../shared/Gauge';
import Gauge from '../shared/Gauge';
import Value from '../shared/Value';
import { format1e, format3f } from '../utils';

dayjs.extend(duration);

const ATTRIBUTES = [
  FacadeAttribute.Current,
  FacadeAttribute.SinceMsg,
  FacadeAttribute.Lifetime,
  FacadeAttribute.CountDown,
  FacadeAttribute.AvgPressure,
  FacadeAttribute.EmittanceH,
  FacadeAttribute.EmittanceV,
];

const CURRENT_COLOR_STOPS: ColorStop[] = [
  { color: '#ec7a08', upTo: 50 },
  { color: '#0088ce', upTo: 140 },
  { color: '#3f9c35' },
];

function BeamCurrent() {
  const states = useLiveAttributes(ATTRIBUTES);

  return (
    <div data-span-2-rows>
      <h2>Beam Current</h2>

      {/* Current */}
      <Gauge
        attributeState={states[FacadeAttribute.Current]}
        colorStops={CURRENT_COLOR_STOPS}
        max={200}
        unit="mA"
      />

      {/* Since Message */}
      <Value attributeState={states[FacadeAttribute.SinceMsg]}>
        {(val) => val}
      </Value>

      {/* Lifetime */}
      <Value label="Lifetime" attributeState={states[FacadeAttribute.Lifetime]}>
        {(val) => dayjs.duration(Math.round(val * 1000)).format('HH:mm:ss')}
      </Value>

      {/* Countdown */}
      <Value
        label="Countdown"
        attributeState={states[FacadeAttribute.CountDown]}
      >
        {(val) => dayjs.duration(Math.round(val * 1000)).format('HH:mm:ss')}
      </Value>

      {/* Average Pressure */}
      <Value
        label="Avg pressure"
        attributeState={states[FacadeAttribute.AvgPressure]}
        unit="mBar"
      >
        {(val) => format1e(val)}
      </Value>

      {/* Emittance H */}
      <Value
        label="Emittance H"
        attributeState={states[FacadeAttribute.EmittanceH]}
        unit="pm"
      >
        {(val) => format3f(val * 1000)}
      </Value>

      {/* Emittance V */}
      <Value
        label="Emittance V"
        attributeState={states[FacadeAttribute.EmittanceV]}
        unit="pm"
      >
        {(val) => format3f(val * 1000)}
      </Value>
    </div>
  );
}

export default BeamCurrent;
