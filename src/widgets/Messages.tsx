import { FiChevronRight } from 'react-icons/fi';

import { FacadeAttribute } from '../data/attributes';
import { useLiveAttributes } from '../data/hooks';
import styles from './Messages.module.css';

const ATTRIBUTES = [
  FacadeAttribute.OperatorMessage,
  FacadeAttribute.OperatorMessageHist,
];

function Messages() {
  const states = useLiveAttributes(ATTRIBUTES);

  const messages = states[FacadeAttribute.OperatorMessageHist]?.value;
  const messagesArr = messages?.split('\n') || [];
  messagesArr.reverse(); // most recent first

  return (
    <details className={styles.root} data-span-2-cols>
      <summary className={styles.latest}>
        <FiChevronRight className={styles.latestChevron} />
        <span className={styles.latestInner}>
          {states[FacadeAttribute.OperatorMessage]?.value}
        </span>
      </summary>
      <div className={styles.history}>
        {messagesArr.map((item) => (
          <p key={item}>{item}</p>
        ))}
      </div>
    </details>
  );
}

export default Messages;
