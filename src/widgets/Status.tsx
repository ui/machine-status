import { FacadeAttribute } from '../data/attributes';
import { useLiveAttributes } from '../data/hooks';
import Value from '../shared/Value';
import styles from './Status.module.css';

const ATTRIBUTES = [FacadeAttribute.SRMode, FacadeAttribute.FillingMode];

const SR_MODES = [
  'Undefined',
  'User Service Mode',
  'Machine Dedicated Time',
  'Shutdown',
  'Safety Test',
  'ID Test',
];

function Status() {
  const states = useLiveAttributes(ATTRIBUTES);

  return (
    <div className={styles.root}>
      <h2 className={styles.heading}>Status</h2>

      {/* SR Mode */}
      <Value attributeState={states[FacadeAttribute.SRMode]}>
        {(val) => SR_MODES[val]}
      </Value>

      {/* Filling Mode */}
      <Value attributeState={states[FacadeAttribute.FillingMode]}>
        {(val) => val}
      </Value>
    </div>
  );
}

export default Status;
