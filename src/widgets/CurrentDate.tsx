import dayjs from 'dayjs';
import { useEffect, useState } from 'react';

import styles from './CurrentDate.module.css';

function CurrentDate() {
  const [date, setDate] = useState(dayjs());

  useEffect(() => {
    const timer = setInterval(() => {
      setDate(dayjs());
    }, 1000);

    return () => {
      clearInterval(timer);
    };
  }, []);

  return (
    <time className={styles.root} dateTime={date.format()} data-span-2-cols>
      {date.format('ddd, D MMM YYYY, HH:mm')}
    </time>
  );
}

export default CurrentDate;
