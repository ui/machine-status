import type { CSSProperties } from 'react';

import { BeamlinesStatusAttribute } from '../data/attributes';
import { useLiveAttributes } from '../data/hooks';
import { BeamlineStatus } from '../data/state';
import StatusGrid from '../shared/StatusGrid';
import styles from './Beamlines.module.css';

const ATTRIBUTES = Object.values(BeamlinesStatusAttribute);

const FALLBACK_STYLES: CSSProperties = { backgroundColor: '#9b9b9b' };
const STATUS_STYLES: Record<BeamlineStatus, CSSProperties> = {
  [BeamlineStatus.FAULT]: { backgroundColor: 'red' },
  [BeamlineStatus.CLOSE]: { backgroundColor: 'white' },
  [BeamlineStatus.INSERT]: { backgroundColor: 'white' },
  [BeamlineStatus.OFF]: { backgroundColor: 'white' },
  [BeamlineStatus.MOVING]: { backgroundColor: '#80a0ff' },
  [BeamlineStatus.STANDBY]: { backgroundColor: 'yellow' },
  [BeamlineStatus.INIT]: { backgroundColor: '#cccc7a' },
  [BeamlineStatus.DISABLE]: { backgroundColor: 'fuchsia' },
  [BeamlineStatus.EXTRACT]: { backgroundColor: 'lime' },
  [BeamlineStatus.ON]: { backgroundColor: 'lime' },
  [BeamlineStatus.OPEN]: { backgroundColor: 'lime' },
  [BeamlineStatus.RUNNING]: { backgroundColor: '#007d00', color: 'white' },
  [BeamlineStatus.ALARM]: { backgroundColor: 'darkorange' },
};

function Beamlines() {
  const states = useLiveAttributes(ATTRIBUTES);

  return (
    <div className={styles.root}>
      <h2>Beamlines</h2>
      <h3 className={styles.gridHeading}>IDs</h3>
      <div className={styles.grid}>
        <StatusGrid
          cols={8}
          entries={[
            {
              label: '1',
              attributeState: states[BeamlinesStatusAttribute.ID1],
            },
            {
              label: '2',
              attributeState: states[BeamlinesStatusAttribute.ID2],
            },
            {
              label: '3',
              attributeState: states[BeamlinesStatusAttribute.ID3],
            },
            undefined,
            undefined,
            {
              label: '6',
              attributeState: states[BeamlinesStatusAttribute.ID6],
            },
            undefined,
            {
              label: '8',
              attributeState: states[BeamlinesStatusAttribute.ID8],
            },
            {
              label: '9',
              attributeState: states[BeamlinesStatusAttribute.ID9],
            },
            {
              label: '10',
              attributeState: states[BeamlinesStatusAttribute.ID10],
            },
            {
              label: '11',
              attributeState: states[BeamlinesStatusAttribute.ID11],
            },
            {
              label: '12',
              attributeState: states[BeamlinesStatusAttribute.ID12],
            },
            {
              label: '13',
              attributeState: states[BeamlinesStatusAttribute.ID13],
            },
            {
              label: '14',
              attributeState: states[BeamlinesStatusAttribute.ID14],
            },
            {
              label: '15',
              attributeState: states[BeamlinesStatusAttribute.ID15],
            },
            {
              label: '16',
              attributeState: states[BeamlinesStatusAttribute.ID16],
            },
            {
              label: '17',
              attributeState: states[BeamlinesStatusAttribute.ID17],
            },
            {
              label: '18',
              attributeState: states[BeamlinesStatusAttribute.ID18],
            },
            {
              label: '19',
              attributeState: states[BeamlinesStatusAttribute.ID19],
            },
            {
              label: '20',
              attributeState: states[BeamlinesStatusAttribute.ID20],
            },
            {
              label: '21',
              attributeState: states[BeamlinesStatusAttribute.ID21],
            },
            {
              label: '22',
              attributeState: states[BeamlinesStatusAttribute.ID22],
            },
            {
              label: '23',
              attributeState: states[BeamlinesStatusAttribute.ID23],
            },
            {
              label: '24',
              attributeState: states[BeamlinesStatusAttribute.ID24],
            },
            undefined,
            {
              label: '26',
              attributeState: states[BeamlinesStatusAttribute.ID26],
            },
            {
              label: '27',
              attributeState: states[BeamlinesStatusAttribute.ID27],
            },
            {
              label: '28',
              attributeState: states[BeamlinesStatusAttribute.ID28],
            },
            {
              label: '29',
              attributeState: states[BeamlinesStatusAttribute.ID29],
            },
            {
              label: '30',
              attributeState: states[BeamlinesStatusAttribute.ID30],
            },
            {
              label: '31',
              attributeState: states[BeamlinesStatusAttribute.ID31],
            },
            {
              label: '32',
              attributeState: states[BeamlinesStatusAttribute.ID32],
            },
          ]}
          statusStyles={STATUS_STYLES}
          fallbackStyles={FALLBACK_STYLES}
        />
      </div>

      <h3 className={styles.gridHeading}>Bendings</h3>
      <div className={styles.grid}>
        <StatusGrid
          cols={8}
          entries={[
            {
              label: '1',
              attributeState: states[BeamlinesStatusAttribute.BM1],
            },
            {
              label: '2',
              attributeState: states[BeamlinesStatusAttribute.BM2],
            },
            undefined,
            undefined,
            {
              label: '5',
              attributeState: states[BeamlinesStatusAttribute.BM5],
            },
            undefined,
            {
              label: '7',
              attributeState: states[BeamlinesStatusAttribute.BM7],
            },
            {
              label: '8',
              attributeState: states[BeamlinesStatusAttribute.BM8],
            },
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            {
              label: '14',
              attributeState: states[BeamlinesStatusAttribute.BM14],
            },
            {
              label: '15',
              attributeState: states[BeamlinesStatusAttribute.BM15],
            },
            {
              label: '16',
              attributeState: states[BeamlinesStatusAttribute.BM16],
            },
            undefined,
            {
              label: '18',
              attributeState: states[BeamlinesStatusAttribute.BM18],
            },
            undefined,
            {
              label: '20',
              attributeState: states[BeamlinesStatusAttribute.BM20],
            },
            undefined,
            undefined,
            {
              label: '23',
              attributeState: states[BeamlinesStatusAttribute.BM23],
            },
            undefined,
            {
              label: '25',
              attributeState: states[BeamlinesStatusAttribute.BM25],
            },
            {
              label: '26',
              attributeState: states[BeamlinesStatusAttribute.BM26],
            },
            undefined,
            {
              label: '28',
              attributeState: states[BeamlinesStatusAttribute.BM28],
            },
            {
              label: '29',
              attributeState: states[BeamlinesStatusAttribute.BM29],
            },
            {
              label: '30',
              attributeState: states[BeamlinesStatusAttribute.BM30],
            },
            {
              label: '31',
              attributeState: states[BeamlinesStatusAttribute.BM31],
            },
            {
              label: '32',
              attributeState: states[BeamlinesStatusAttribute.BM32],
            },
          ]}
          statusStyles={STATUS_STYLES}
          fallbackStyles={FALLBACK_STYLES}
        />
      </div>
    </div>
  );
}

export default Beamlines;
