import { useDebouncedState } from '@react-hookz/web';
import { pick } from 'lodash';
import { useEffect, useMemo } from 'react';
import { useLazyLoadQuery, useSubscription } from 'react-relay';
import { shallow } from 'zustand/shallow';

import { assertNonNull, isNonNull } from '../utils';
import type { HistoricalDataQuery } from './__generated__/HistoricalDataQuery.graphql';
import type {
  LiveAttributesSubscription,
  LiveAttributesSubscription$data,
} from './__generated__/LiveAttributesSubscription.graphql';
import type { Attribute } from './attributes';
import { BeamlinesStatusAttribute, FacadeAttribute } from './attributes';
import historicalDataQuery from './HistoricalDataQuery';
import { useLiveAttributesStore } from './live-store';
import liveAttributesSubscription from './LiveAttributesSubscription';
import type { HistAttributeState, LiveAttributesState } from './state';

export const LIVE_ATTRIBUTES = Object.values({
  ...FacadeAttribute,
  ...BeamlinesStatusAttribute,
});

/**
 * Subscribe to state updates of multiple attributes together.
 * Consecutive state updates that occur within a few ms of one another are batched
 * together for performance and usability.
 */
export function useLiveAttributes(
  names: Attribute[]
): Partial<LiveAttributesState> {
  const [states, setStates] = useDebouncedState<Partial<LiveAttributesState>>(
    () => pick(useLiveAttributesStore.getState().attributes, names),
    50,
    150
  );

  useEffect(
    () => {
      // https://github.com/pmndrs/zustand/discussions/1179
      return useLiveAttributesStore.subscribe((state) => {
        setStates((prevStates) => {
          // Pick relevant attribute states from store
          const newStates = pick(state.attributes, names);

          // Skip unnecessary re-renders when an unrelated attribute in the store gets updated
          return shallow(prevStates, newStates) ? prevStates : newStates;
        });
      });
    },
    names // eslint-disable-line react-hooks/exhaustive-deps
  );

  return states;
}

/**
 * Fetch historical state updates of the given attribute.
 */
export function useAttributeHistory(
  attribute: Attribute,
  count: number,
  fetchKey: number | undefined
): HistAttributeState[] {
  const lastSlash = attribute.lastIndexOf('/');
  const device = attribute.slice(0, lastSlash);
  const attrName = attribute.slice(lastSlash + 1);

  const response = useLazyLoadQuery<HistoricalDataQuery>(
    historicalDataQuery,
    { device, attrName, count },
    { fetchKey, fetchPolicy: 'network-only' }
  );

  const { values, error } = response.device;
  if (error !== null) {
    throw new Error(error.filter((e) => e !== null).join(' ; '));
  }

  assertNonNull(values);

  // Cast `value` and remove `readonly` modifiers added by relay-compiler
  return values as HistAttributeState[];
}

/**
 * Establish a WebSocket connection to subscribe to live attributes updates.
 * This hook is called once, when the app first renders, so as to open a single
 * socket connection.
 *
 * Updates received from the server are dispatched to the live store,
 * which can then be subscribed to with hook `useLiveAttributes`.
 */
export function useLiveAttributesSubscription() {
  const setAttribute = useLiveAttributesStore((state) => state.setAttribute);

  const config = useMemo(
    () => ({
      subscription: liveAttributesSubscription,
      variables: { attributes: LIVE_ATTRIBUTES },
      onNext: (next: unknown) => {
        const { subscribe } = next as LiveAttributesSubscription$data;
        assertNonNull(subscribe, 'Expected subscription message');

        const { full_name, payload, subscription_error } = subscribe;

        // Store subscription error
        if (subscription_error) {
          setAttribute(full_name, { ok: false, error: subscription_error });
          return;
        }

        assertNonNull(
          payload,
          "Expected message payload since there's no error"
        );

        // Store payload error
        if (payload.error) {
          setAttribute(full_name, {
            ok: false,
            error: payload.error.filter(isNonNull).join('; '),
          });
          return;
        }

        // Store attribute value
        setAttribute(full_name, {
          ok: true,
          value: payload.value,
          timestamp: payload.timestamp,
        });
      },
      onError: console.error, // eslint-disable-line no-console
    }),
    [setAttribute]
  );

  useSubscription<LiveAttributesSubscription>(config);
}
