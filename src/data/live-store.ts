import { create } from 'zustand';

import type { LiveAttributesState, LiveAttributeState } from './state';

interface LiveAttributesStore {
  attributes: Partial<LiveAttributesState>;
  setAttribute: (attr: string, value: LiveAttributeState) => void;
}

export const useLiveAttributesStore = create<LiveAttributesStore>()((set) => ({
  attributes: {},
  setAttribute: (name, value) => {
    set((state) => ({ attributes: { ...state.attributes, [name]: value } }));
  },
}));
