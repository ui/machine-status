import { graphql } from 'relay-runtime';

export default graphql`
  query HistoricalDataQuery(
    $device: String!
    $attrName: String!
    $count: Int!
  ) {
    device(name: $device) {
      values: read_attribute_hist(name: $attrName, depth: $count) {
        value
        timestamp
      }
      info: attribute_info(names: [$attrName]) {
        unit
        display_unit
      }
      error
    }
  }
`;
