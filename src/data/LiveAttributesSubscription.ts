import { graphql } from 'relay-runtime';

export default graphql`
  subscription LiveAttributesSubscription($attributes: [String!]!) {
    subscribe(attNames: $attributes, modes: CHANGE_PERIODIC) {
      full_name
      payload: value {
        value(resolveEnum: true)
        timestamp
        error
      }
      subscription_error
    }
  }
`;
