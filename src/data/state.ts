import type { BeamlinesStatusAttribute, FacadeAttribute } from './attributes';

export type LiveAttributesState = LiveFacadeAttributesState &
  LiveBeamlinesStatusAttributesState;

export interface LiveFacadeAttributesState {
  [FacadeAttribute.Current]: LiveAttributeState<number>;
  [FacadeAttribute.CurrentHist]: LiveAttributeState<number>;
  [FacadeAttribute.SinceMsg]: LiveAttributeState<string>;
  [FacadeAttribute.Lifetime]: LiveAttributeState<number>;
  [FacadeAttribute.LifetimeHist]: LiveAttributeState<number>;
  [FacadeAttribute.CountDown]: LiveAttributeState<number>;
  [FacadeAttribute.AvgPressure]: LiveAttributeState<number>;
  [FacadeAttribute.EmittanceH]: LiveAttributeState<number>;
  [FacadeAttribute.EmittanceHHist]: LiveAttributeState<number>;
  [FacadeAttribute.EmittanceV]: LiveAttributeState<number>;
  [FacadeAttribute.EmittanceVHist]: LiveAttributeState<number>;
  [FacadeAttribute.SRMode]: LiveAttributeState<number>;
  [FacadeAttribute.FillingMode]: LiveAttributeState<string>;
  [FacadeAttribute.OperatorMessage]: LiveAttributeState<string>;
  [FacadeAttribute.OperatorMessageHist]: LiveAttributeState<string>;
}

export type LiveBeamlinesStatusAttributesState = Record<
  BeamlinesStatusAttribute,
  LiveAttributeState<BeamlineStatus>
>;

export enum BeamlineStatus {
  FAULT = 'FAULT',
  CLOSE = 'CLOSE',
  INSERT = 'INSERT',
  OFF = 'OFF',
  MOVING = 'MOVING',
  STANDBY = 'STANDBY',
  INIT = 'INIT',
  DISABLE = 'DISABLE',
  EXTRACT = 'EXTRACT',
  ON = 'ON',
  OPEN = 'OPEN',
  RUNNING = 'RUNNING',
  ALARM = 'ALARM',
}

/*
 * Live attribute state object received from GraphQL server.
 * Can have either `value` or `error` but not both; check `ok` before accessing either.
 * https://www.typescriptlang.org/docs/handbook/2/narrowing.html#discriminated-unions
 */
export type LiveAttributeState<T = unknown> =
  | LiveAttributeValidState<T>
  | LiveAttributeErrorState;

export interface LiveAttributeValidState<T> {
  ok: true;
  value: T;
  timestamp: number;
  error?: never;
}

export interface LiveAttributeErrorState {
  ok: false;
  value?: never;
  timestamp?: never;
  error: string;
}

/*
 * Attribute state object received when fetching historical numeric data for plotting.
 */
export interface HistAttributeState {
  value: number;
  timestamp: number;
}
