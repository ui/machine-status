import type { FallbackProps } from 'react-error-boundary';

function ErrorFallback(props: FallbackProps) {
  const { error } = props;

  return <p data-error>{error.message}</p>;
}

export default ErrorFallback;
