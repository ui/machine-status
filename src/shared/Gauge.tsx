import type { CSSProperties } from 'react';

import type { LiveAttributeState } from '../data/state';
import { format3f } from '../utils';
import styles from './Gauge.module.css';

function getColor(value: number, colorStops: ColorStop[]): string {
  for (const { upTo, color } of colorStops) {
    if (upTo === undefined || value < upTo) {
      return color;
    }
  }

  throw new Error(`Expected color stop for value \`${value}\``);
}

export interface ColorStop {
  color: string;
  upTo?: number /* upper bound (exclusive) */;
}

interface Props {
  attributeState: LiveAttributeState<number> | undefined;
  colorStops: ColorStop[];
  max: number;
  unit: string;
}

function Gauge(props: Props) {
  const { attributeState, colorStops, max, unit } = props;

  if (!attributeState) {
    // Not received yet
    return (
      <div className={styles.root}>
        <div className={styles.gauge} />
        <p className={styles.legend}>
          <span className={styles.unit}>{unit}</span>
          <span className={styles.min}>0</span>
          <span className={styles.max}>{max}</span>
        </p>
      </div>
    );
  }

  const { ok, value, error } = attributeState;
  return (
    <div
      className={styles.root}
      style={
        ok
          ? ({
              '--gauge-value': Math.min(Math.max(value / max, 0), 1),
              '--gauge-color': getColor(value, colorStops),
            } as CSSProperties)
          : undefined
      }
    >
      <div className={styles.gauge}>
        <p className={styles.value} data-error={error}>
          {attributeState &&
            (ok ? format3f(value) : <abbr title={error}>###</abbr>)}
        </p>
      </div>
      <p className={styles.legend}>
        <span className={styles.unit}>{unit}</span>
        <span className={styles.min}>0</span>
        <span className={styles.max}>{max}</span>
      </p>
    </div>
  );
}

export default Gauge;
