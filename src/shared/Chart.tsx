import { useMeasure } from '@react-hookz/web';
import {
  AreaSeries,
  Axis,
  Grid,
  lightTheme,
  Tooltip,
  XYChart,
} from '@visx/xychart';
import dayjs from 'dayjs';

import type { HistAttributeState } from '../data/state';
import { format2f } from '../utils';
import styles from './Chart.module.css';

interface Props {
  values: HistAttributeState[];
  yAxisMaxConfig?: number[];
  label: string;
  unit: string;
}

const xAccessor = (item: HistAttributeState) => new Date(item.timestamp * 1000);
const yAccessor = (item: HistAttributeState) => item.value;

const Y_MAX_CONFIG = [
  2, 4, 6, 8, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 150, 200, 250, 300, 350,
  400, 450, 500, 550, 600, 700, 800, 900, 1000, 1500, 2000, 2500, 3000, 3500,
  4000, 4500, 5000, 5500, 6000,
];

function Chart(props: Props) {
  const { values, yAxisMaxConfig = Y_MAX_CONFIG, label, unit } = props;
  const [size, ref] = useMeasure<HTMLDivElement>();
  const now = dayjs();

  const yMax = values.reduce((max, next) => {
    const val = yAccessor(next);
    return Number.isNaN(val) ? max : Math.max(val, max);
  }, -Infinity);

  const yGridMax = Number.isFinite(yMax)
    ? yAxisMaxConfig.find((x: number) => x >= yMax) ||
      yAxisMaxConfig[yAxisMaxConfig.length - 1]
    : yAxisMaxConfig[1];

  return (
    <div ref={ref} className={styles.chart}>
      {size && (
        <XYChart
          width={size.width}
          height={300}
          xScale={{
            type: 'time',
            domain: [now.subtract(8, 'hours').valueOf(), now.valueOf()],
          }}
          yScale={{ type: 'linear', domain: [0, yGridMax] }}
          theme={{ ...lightTheme, colors: ['#4aaadd'] }}
          margin={{ left: 70, right: 0, top: 15, bottom: 20 }}
        >
          <Axis
            orientation="left"
            numTicks={6}
            label={`${label} (${unit})`}
            labelOffset={30}
          />
          <Axis
            orientation="bottom"
            numTicks={6}
            tickFormat={(d: Date) => dayjs(d).format('HH:mm')}
          />
          <Grid numTicks={6} />
          <AreaSeries
            dataKey="values"
            data={values}
            xAccessor={xAccessor}
            yAccessor={yAccessor}
            fillOpacity={0.4}
          />

          <Tooltip<HistAttributeState>
            showVerticalCrosshair
            showDatumGlyph
            snapTooltipToDatumX
            offsetTop={-20}
            offsetLeft={20}
            renderTooltip={({ tooltipData }) => {
              const state = tooltipData?.nearestDatum?.datum;
              if (!state) {
                return null;
              }

              const date = dayjs(xAccessor(state)).format('DD MMM - HH:mm');
              const value = yAccessor(state);

              return (
                <>
                  <div className={styles.date}>{date}</div>
                  <div className={styles.value}>
                    {label}: {format2f(value)} {unit}
                  </div>
                </>
              );
            }}
          />
        </XYChart>
      )}
    </div>
  );
}

export default Chart;
