import { Tooltip } from '@reach/tooltip';
import type { CSSProperties } from 'react';

import type { LiveAttributeState } from '../data/state';
import styles from './StatusGrid.module.css';

interface Entry<T> {
  label: string;
  attributeState: LiveAttributeState<T> | undefined;
}

interface Props<T extends string> {
  cols: number;
  entries: (Entry<T> | undefined)[];
  statusStyles: Record<T, CSSProperties>;
  fallbackStyles: CSSProperties;
}

function StatusGrid<T extends string>(props: Props<T>) {
  const { cols, entries, statusStyles, fallbackStyles } = props;

  return (
    <div
      className={styles.grid}
      style={{ gridTemplateColumns: `repeat(${cols}, 1fr)` }}
    >
      {entries.map((entry, index) => {
        const key = `${index}_${entry?.label || ''}`;

        if (!entry) {
          return <span key={key} />;
        }

        const { label, attributeState } = entry;
        if (!attributeState) {
          return <span key={key}>{label}</span>; // not received yet
        }

        const { ok, value, error } = attributeState;
        return (
          <Tooltip
            key={key}
            label={ok ? `${label}: ${value}` : `Error: ${error}`}
          >
            <span
              style={ok ? statusStyles[value] || fallbackStyles : {}}
              data-error={error}
            >
              {ok ? label : '#'}
            </span>
          </Tooltip>
        );
      })}
    </div>
  );
}

export default StatusGrid;
