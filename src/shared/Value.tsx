import type { ReactNode } from 'react';

import type { LiveAttributeState } from '../data/state';
import styles from './Value.module.css';

interface Props<T> {
  label?: string;
  attributeState: LiveAttributeState<T> | undefined;
  unit?: string;
  children: (value: T) => ReactNode;
}

function Value<T>(props: Props<T>) {
  const { label, attributeState, unit, children } = props;

  if (!attributeState) {
    return (
      <p className={styles.info}>
        {/* Not received yet; render label + non-breakable space to avoid layout shift once value becomes available. */}
        {label && <span className={styles.label}>{label} </span>}
      </p>
    );
  }

  const { ok, value, error } = attributeState;
  return (
    <p className={styles.info}>
      {label && <span className={styles.label}>{label} </span>}

      <span className={styles.value} data-error={error}>
        {ok ? children(value) : <abbr title={error}>###</abbr>}
      </span>

      {unit && <span className={styles.unit}>{unit} </span>}
    </p>
  );
}

export default Value;
