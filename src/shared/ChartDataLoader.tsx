import type { ReactNode } from 'react';
import { useDeferredValue } from 'react';
import { useEffect, useState } from 'react';

import type { Attribute } from '../data/attributes';
import { useAttributeHistory } from '../data/hooks';
import { useLiveAttributesStore } from '../data/live-store';
import type { HistAttributeState } from '../data/state';

const TIMEFRAME = 8; // 8 hours

interface Props {
  attribute: Attribute;
  children: (values: HistAttributeState[]) => ReactNode;
}

function ChartDataLoader(props: Props) {
  const { attribute, children } = props;

  const [timestamp, setTimestamp] = useState<number>();
  const deferredTimestamp = useDeferredValue(timestamp); // don't show suspense fallback again while re-fecthing history

  const pastStates = useAttributeHistory(
    attribute,
    TIMEFRAME * 60,
    deferredTimestamp // re-fetch when latest timestamp changes
  );

  useEffect(() => {
    // Subscribe to history attribute and store latest timestamp in order to trigger full re-fetch
    return useLiveAttributesStore.subscribe((state) => {
      setTimestamp(state.attributes[attribute]?.timestamp);
    });
  }, [attribute]);

  return <>{children(pastStates)}</>;
}

export default ChartDataLoader;
