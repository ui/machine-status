import type { ExecutionResult } from 'graphql';
import type { Sink } from 'graphql-ws';
import { createClient } from 'graphql-ws';
import type {
  GraphQLResponse,
  RequestParameters,
  Variables,
} from 'relay-runtime';
import { Observable } from 'relay-runtime';
import { Environment, Network, RecordSource, Store } from 'relay-runtime';
import type { RelayObservable } from 'relay-runtime/lib/network/RelayObservable';
import wretch from 'wretch';

const TANGO_HOST: string = import.meta.env.VITE_TANGO_GQL_HOST || '';
const HTTP_SECURE: string =
  import.meta.env.VITE_HTTP_SECURE === 'true' ? 's' : '';

const wsClient = createClient({
  url: `ws${HTTP_SECURE}://${TANGO_HOST}/graphql-ws`,
});

async function fetchQuery(
  params: RequestParameters,
  variables: Variables
): Promise<GraphQLResponse> {
  const { text: query } = params;

  if (query === null) {
    throw new Error('Operation text cannot be empty');
  }

  return wretch(`http${HTTP_SECURE}://${TANGO_HOST}/graphql`)
    .accept('application/json')
    .post({ query, variables })
    .json();
}

function subscribe(
  operation: RequestParameters,
  variables: Variables
): RelayObservable<GraphQLResponse> {
  const { name: operationName, text: query } = operation;

  return Observable.create((sink) => {
    if (!query) {
      sink.error(new Error('Operation text cannot be empty'));
      return undefined;
    }

    return wsClient.subscribe(
      { operationName, query, variables },
      sink as Sink<ExecutionResult>
    );
  });
}

const relayEnv = new Environment({
  network: Network.create(fetchQuery, subscribe),
  store: new Store(new RecordSource()),
});

export default relayEnv;
